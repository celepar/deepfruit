# Deep Fruit Detection in Orchards Paper Reproduction
## Group 24 | Haydar Yildiz, Caspar van Venrooij

Welcome to our GitLab repository for the reproduction project for the course CS4240 Deep Learning (2020/21 Q3).

Below is a link to our blog post and our code is in the Jupyter notebook named [deepfruit.ipynb](/deepfruit.ipynb).

All the apple images and annotations are in the directory [Apples](/Apples). We used a few helper functions which are in the [Helper_files](/Helper_files) directory and some of the saved [models](/Saved_models)/[results](/Saved_results) are in their own respective directories.

[Link to the Blog Post](/Deep_Fruit_Blog_Post.pdf )
